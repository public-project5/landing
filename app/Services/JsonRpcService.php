<?php

declare(strict_types = 1);

namespace App\Services;

use App\Soa\JsonRpc;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;

abstract class JsonRpcService
{

    /**
     * урл до конечной точки апи
     * @var string
     */
    private string $apiUrl;

    /**
     * токен авторизации
     * @var string
     */
    private string $token;

    public function __construct()
    {
        $this->apiUrl = env('JSON_RPC_API_BASE_URL') . $this->endpoint();
        $this->token = env('JSON_RPC_API_TOKEN', 'API_TOKEN');
    }

    /**
     * отправляет json-rpc запрос на сервер api
     * @param JsonRpc $jsonRpc
     * @return array
     */
    public function send(JsonRpc $jsonRpc) : array
    {
        try {
            return Http::withHeaders(['Authorization' => "Bearer {$this->token}"])->post(
                $this->apiUrl,
                $jsonRpc->query()
            )->json();
        } catch (ConnectionException $ex) {
            return ['error' => ['message' => $ex->getMessage(), 'code' => $ex->getCode()]];
        }
    }

    /**
     * метод возвращает конечную точку для конкретной реализации сервиса
     * @return string
     */
    abstract protected function endpoint() : string;

}
