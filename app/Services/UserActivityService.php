<?php

declare(strict_types = 1);

namespace App\Services;

use App\Facades\JsonRpc;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;

class UserActivityService extends JsonRpcService
{
    /**
     * конечная точка апи
     */
    public const ENDPOINT = '/user/activity';

    /**
     * делает запрос к серверу на добавление активности пользователя
     * @param string $pageUrl урл открытой страницы пользователем
     * @param string $dateTime время в формате
     * @param $id
     * @return array
     */
    public function sendUserActivity(string $pageUrl, string $dateTime, $id = 1) : array
    {
        return $this->send(
            JsonRpc::method('UserActivity@add')
                ->params([
                    'url' => $pageUrl,
                    'datetime' => (new Carbon())->setTimeFromTimeString($dateTime)->format('d.m.Y h:i:s'),
                ])
                ->id($id)
        );
    }

    /**
     * делает запрос к серверу на получение списка активностей пользователя с пагинацией
     * @param int $page текущая запрашиваемая страница
     * @param int $limit кол-во элементов на странице
     * @return array
     */
    public function getUserActivities(int $page = 1, int $limit = 10, string $groupBy = '') : array
    {
        $params = [
            'page' => $page,
            'limit' => $limit,
        ];

        if ($groupBy) {
            $params['group_by'] = $groupBy;
        }

        return $this->send(
            JsonRpc::method('UserActivity@get')
                ->params($params)
        );
    }

    /**
     * возвращает конечную точку апи
     * @return string
     */
    function endpoint() : string
    {
        return self::ENDPOINT;
    }

}
