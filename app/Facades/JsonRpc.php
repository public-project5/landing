<?php

declare(strict_types = 1);

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \App\Soa\JsonRpc id(int $id)
 * @method static \App\Soa\JsonRpc method(string $method)
 * @method static \App\Soa\JsonRpc params(array $params)
 * @method static array query()
 * @method static string jsonQuery()
 *
 * @see \App\Soa\JsonRpc
 */

class JsonRpc extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'JsonRpc';
    }
}
