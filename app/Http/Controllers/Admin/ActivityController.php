<?php

namespace App\Http\Controllers\Admin;

use App\Facades\JsonRpc;
use App\Http\Controllers\Controller;
use App\Services\UserActivityService;
use Bitrix\Crm\Preview\Route;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ActivityController extends Controller
{

    const DEFAULT_GROUP_BY = 'url';

    private UserActivityService $userActivityService;

    public function __construct(UserActivityService $userActivityService)
    {
        $this->userActivityService = $userActivityService;
    }

    public function index(Request $request)
    {
        $userActivities = $this->userActivityService->getUserActivities(
            $request->get('page', 1),
            $request->get('limit', 10),
            self::DEFAULT_GROUP_BY
        )['result'];

        $data['userActivities'] = new LengthAwarePaginator(
            $userActivities['items'],
            $userActivities['total'],
            $userActivities['limit'],
            $userActivities['page'],
            ['path' => \route('admin.activity')]
        );

        return view('admin.activity', $data);
    }
}
