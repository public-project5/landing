<?php

namespace App\Http\Middleware;

use App\Services\UserActivityService;
use App\Soa\Exceptions\JsonRpcResponseException;
use App\Soa\JsonRpc;
use Closure;
use Illuminate\Http\Request;

class UserActivity
{

    private UserActivityService $userActivityService;
    private string $timezone;

    public function __construct(UserActivityService $userActivityService)
    {
        $this->userActivityService = $userActivityService;
        $this->timezone = env('DEFAULT_TIMEZONE', 'Europe/Moscow');
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        //TODO: не отправлять напрямую на сервер, а добавлять в очердь
        $response = $this->userActivityService->sendUserActivity(
            $request->fullUrl(),
            now($this->timezone)->format('d.m.Y h:i:s')
        );
        if (isset($response['error'])) {
            throw new JsonRpcResponseException($response['error']['message'], $response['error']['code']);
        }
        return $next($request);
    }
}
