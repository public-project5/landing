<?php

declare(strict_types = 1);

namespace App\Soa\Contracts;

interface JsonRpc
{
    public function id(int $id = 1);

    public function method(string $method);

    public function params(array $params);

    public function query() : array;

    public function jsonQuery() : string;
}
