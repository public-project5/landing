<?php

declare(strict_types = 1);

namespace App\Soa;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * класс создаёт объект JsonRpc, заполняет его данными и возвращает
 * массив или готовую json строку формата json-rpc 2.0
 * для удобства использования существует фасад @see \App\Facades\JsonRpc
 */
class JsonRpc implements \App\Soa\Contracts\JsonRpc
{
    /**
     * id json-rpc
     * @var int
     */
    private int $id = 1;
    /**
     * метод json-rpc вида <ProcedureName@method>
     * @var string
     */
    private string $method = '';
    /**
     * параметры передаваемые в json-rpc
     * @var array
     */
    private array $params = [];
    /**
     * версия json-rpc
     */
    const VERSION = '2.0';

    /**
     * устанавливает id json-rpc
     * @param int $id необязательный параметр по-умолчанию равен 1
     * @return $this возвращает объект \App\Soa\JsonRpc
     */
    public function id(int $id = 1)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * устанавливает метод json-rpc
     * @param string $method метод json-rpc вида <ProcedureName@method>
     * @return $this возвращает объект \App\Soa\JsonRpc
     */
    public function method(string $method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * устанавливает параметры json-rpc
     * @param array $params массив параметров вида [ключ=>значение]
     * @return $this возвращает объект \App\Soa\JsonRpc
     */
    public function params(array $params)
    {
        $this->params = $params;
        return $this;
    }

    /**
     * возвращает массив с данными для формирования одного json-rpc объекта запроса
     * проверяет заполненность обязательный полей, если поля не заполнены бросает исключение
     * если поле params не заполнено, то оно не добавляется в тело объекта
     * @return array
     * @throws Exceptions\JsonRpсInvalidParamValueException
     */
    public function query() : array
    {
        foreach (get_object_vars($this) as $name => $value) {
            if ($name != 'params' && !$value) {
                throw new \App\Soa\Exceptions\JsonRpсInvalidParamValueException($name, $value);
            }
        }
        $query['jsonrpc'] = self::VERSION;
        $query['method'] = $this->method;
        if ($this->params) {
            $query['params'] = $this->params;
        }
        $query['id'] = $this->id;

        return $query;
    }

    /**
     * возвращает заполненый данными json-rpc объект запроса в виде json строки
     * @return string
     * @throws Exceptions\JsonRpсInvalidParamValueException
     */
    public function jsonQuery() : string
    {
        return JsonResource::make($this->query())->toJson();
    }
}
