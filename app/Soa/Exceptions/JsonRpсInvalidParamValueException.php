<?php

declare(strict_types = 1);

namespace App\Soa\Exceptions;

class JsonRpсInvalidParamValueException extends \Exception
{
    public function __construct(string $paramName, string|null $paramValue)
    {
        $message = "Неверное значение {$paramName} = '{$paramValue}'";
        parent::__construct($message);
    }
}
