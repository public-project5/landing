<?php

declare(strict_types = 1);

namespace App\Soa\Exceptions;

class JsonRpcResponseException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct("Удалённый сервер вернул ошибку: {$message}", $code, $previous);
    }
}
