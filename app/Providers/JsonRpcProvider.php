<?php

namespace App\Providers;

use App\Soa\JsonRpc;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class JsonRpcProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('JsonRpc', function () {
            return new JsonRpc();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
