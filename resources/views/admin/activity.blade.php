@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-4">URL</div>
                            <div class="col-4">Кол-во посещений</div>
                            <div class="col-4">Время последнего</div>
                            @foreach($userActivities as $activity)
                                <div class="col-4">{{$activity['url']}} </div>
                                <div class="col-4">{{$activity['count']}}</div>
                                <div class="col-4">{{$activity['last_visit']}}</div>
                            @endforeach
                        </div>
                        <div class="d-flex justify-content-center">
                            {!! $userActivities->links() !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
